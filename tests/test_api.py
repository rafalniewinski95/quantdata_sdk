"""
tests.test_api.py
~~~~~~~~~~~~~~~~~~~~

Test suite for the api.py
"""


from quantdata_sdk.api import QuantumDataApi


class TestApi:
    """ Test for api module """

    API_KEY = "AWESOMEKEY"

    def test_get_companies(self):
        api = QuantumDataApi(self.API_KEY)
        response = api.get_companies()
        assert type(response) == list

    def test_get_quotations(self):
        api = QuantumDataApi(self.API_KEY)
        response = api.get_quotations("KGHM")
        assert type(response) == list

    def test_get_reports(self):
        api = QuantumDataApi(self.API_KEY)
        response = api.get_reports("KGHM")
        assert type(response) == list
