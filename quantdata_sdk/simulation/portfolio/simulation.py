from collections import namedtuple
from .utils import rand_weights


def sim_markovitz(n_portfolios=500, entity_returns=None):
    if entity_returns is None:
        raise Exception("entity returns needed!")

    PortfolioMin = namedtuple('PortfolioMin', ['mu', 'sigma', 'weights'])
    PortfolioMax = namedtuple('PortfolioMax', ['mu', 'sigma', 'weights'])

    def random_portfolio(entity_returns):
        """
        Returns the mean and standard deviation of returns for a random portfolio
        """

        p = np.asmatrix(np.mean(entity_returns, axis=1))
        weights = np.asmatrix(rand_weights(entity_returns.shape[0]))
        C = np.asmatrix(np.cov(entity_returns))

        mu = weights * p.T
        sigma = np.sqrt(weights * C * weights.T)

        # This recursion reduces outliers to keep plots pretty
        if sigma > 2:
            return random_portfolio(entity_returns)

        cleaned_weights = ",".join(
            "{:10.4f}".format(float(w)) for w in np.squeeze(np.asarray(weights))
        )
        return mu, sigma, cleaned_weights

    means, stds, cleaned_weights = np.column_stack([
        random_portfolio(return_vec)
        for _ in range(n_portfolios)
    ])

    plt.plot(stds, means, 'o', markersize=5)
    plt.xlabel('std')
    plt.ylabel('mean')
    plt.title(
        'Mean and standard deviation of returns of randomly generated portfolios')

    df = pd.DataFrame(
        data={'means': means, 'stds': stds, 'weights': cleaned_weights})  # noqa
    max_means, max_stds, max_weights = df.max()
    max_portfolio = PortfolioMax(max_means, max_stds, max_weights)
    df = pd.DataFrame(data={'stds': list(stds), 'means': list(means),
                            'weights': cleaned_weights})  # noqa
    min_stds, min_means, min_weights = df.min()
    min_portfolio = PortfolioMin(min_means, min_stds, min_weights)
    return min_portfolio, max_portfolio


def sim_capm(n_portfolios=500, entity_returns=None, market_returns=None,
             free_return=0):
    if entity_returns is None or not market_returns:
        raise Exception("entity returns needed!")

    PortfolioMin = namedtuple('PortfolioMin', ['mu', 'sigma', 'weights'])
    PortfolioMax = namedtuple('PortfolioMax', ['mu', 'sigma', 'weights'])

    def random_portfolio(beta, entity_returns):
        """
        Returns the mean and standard deviation of returns for a random portfolio
        """
        betas = []
        means = []
        weights = []
        for portfolio in range(n_portfolios):
            weights = np.asmatrix(rand_weights(entity_returns.shape[0]))
            portfolio_beta = beta.values() * weights
            rm = np.mean(entity_returns, axis=1) * 252
            # in year scale
            forecast_return = free_return + (portfolio_beta * (rm - free_return))
            cleaned_weights = ",".join(
                "{:10.4f}".format(float(w)) for w in
                np.squeeze(np.asarray(weights))
            )
            weights.append(cleaned_weights)
            betas.append(portfolio_beta)
            means.append(forecast_return)

        return beta, means, weights

    if len(entity_returns.columns) == 1:
        """ capm model for single company  """
        beta, alpha = np.polyfit(
            entity_returns,
            market_returns, 1
        )
        entity_returns = entity_returns.ix[:, 0]
        rm = np.mean(entity_returns, axis=1) * 252
        # in year scale
        forecast_return = free_return + (beta * (rm - free_return))
        stocks_daily_return.plot(
            kind='scatter', x='market returns', y='single company returns'
        )
        plt.plot(market_returns, beta * market_returns + alpha, '-', color='r')
        return forecast_return, beta

    beta = {}
    alpha = {}
    for column in entity_returns.columns:
        entity_returns.plot(kind='scatter', x='market returns', y=column)
        b, a = np.polyfit(market_returns, entity_returns[column], 1)
        plt.plot(market_returns, b * market_returns + a, '-', color='r')
        beta[i] = b
        alpha[i] = a
        plt.show()

    for key, value in beta.items():
        print(f"Beta: {key}, {value}")

    beta, forecast_returns, weights = random_portfolio(beta, entity_returns)
    df = pd.DataFrame(data={'beta': beta, 'forecast returns': forecast_returns, 'weights': weights}) # noqa
    max_forecast, max_beta, max_weights = df.max()
    min_forecast, min_beta, min_weights = df.min()
    max_portfolio = PortfolioMax(max_beta, max_forecast, max_weights)
    min_portfolio = PortfolioMin(min_beta, min_forecast, min_weights)

    plt.plot(forecast_returns, beta, 'o', markersize=5)
    plt.xlabel('beta')
    plt.ylabel('forecast')
    return min_portfolio, max_portfolio
