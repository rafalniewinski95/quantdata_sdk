import pandas as pd
from ta.momentum import rsi, stoch, roc
from ta.trend import ema_indicator, sma_indicator, macd_signal, macd, \
    ichimoku_a, ichimoku_base_line, ichimoku_conversion_line, \
    psar_up, aroon_down, aroon_up, wma_indicator
from ta.volatility import bollinger_hband, bollinger_lband, \
    keltner_channel_lband, keltner_channel_hband, \
    donchian_channel_lband, donchian_channel_hband
from .fama import FamaMixin


class ATBasicSimulation:

    def momentum_indicator_long(self, indicator, oversold, overbought,
                                indicator_length):
        capital = self.CAPITAL
        buy = False
        for index, price in enumerate(self.closed_returns[indicator_length:]):
            if indicator[index] < oversold and not buy:
                entities = round(capital / price)
                buy = True
            if indicator[index] > overbought and buy:
                result = entities * price
                capital += (result - capital)
                buy = False
        return (capital / self.CAPITAL) * 100
    
    def trend_indicator_long(self, indicator, indicator_length):
        capital = self.CAPITAL
        buy = False
        for index, price in enumerate(self.closed_returns[indicator_length:]):
            if price > indicator[index] and not buy:
                entities = round(capital / price)
                buy = True
            if price < indicator[index] and buy:
                result = entities * price
                capital += (result - capital)
                buy = False
                
        return (capital / self.CAPITAL) * 100

    def trend_double_indicator_long(
            self, base_indicator, signal_indicator, indicator_length):
        capital = self.CAPITAL
        buy = False
        for index, price in enumerate(self.closed_returns[indicator_length:]):
            if signal_indicator[index] > base_indicator[index] and not buy:
                entities = round(capital / price)
                buy = True
            if signal_indicator[index] < base_indicator[index] and buy:
                result = entities * price
                capital += (result - capital)
                buy = False

        return (capital / self.CAPITAL) * 100

    def trend_ichimoku_long(
            self, tenkan_sen, kijun_sen, ichimoku_a, indicator_length):
        capital = self.CAPITAL
        buy = False
        for index, price in enumerate(self.closed_returns[indicator_length:]):
            # check if signal
            if tenkan_sen[index] > kijun_sen[index] and not buy:
                # check if strong buy signal
                if price > ichimoku_a[index]:
                    entities = round(capital / price)
                    buy = True
            if tenkan_sen[index] < kijun_sen[index] and buy:
                result = entities * price
                capital += (result - capital)
                buy = False

        return (capital / self.CAPITAL) * 100

    def volatility_long(self, lower_band, higher_band, WINDOW):
        capital = self.CAPITAL
        buy = False
        for index, price in enumerate(self.closed_returns[WINDOW:]):
            # check if signal
            if price < lower_band[index] and not buy:
                entities = round(capital / price)
                buy = True
            if price > higher_band[index] and buy:
                result = entities * price
                capital += (result - capital)
                buy = False

        return (capital / self.CAPITAL) * 100


class ATSimulation(FamaMixin, ATBasicSimulation):
    CAPITAL = 1000000
    RSI_LENGTH = 14
    STOCHASTIC_LOW = 3
    STOCHASTIC_HIGH = 14
    ROC_LENGTH = 20
    EMA_LENGTH = 14
    SMA_LENGTH = 14
    WMA_LENGTH = 14
    MACD_SLOW = 26
    MACD_FAST = 12
    MACD_SIGN = 9

    ICHIMOKU_WINDOW_1 = 9
    ICHIMOKU_WINDOW_2 = 26

    AROON_DOWN = 25
    AROON_UP = 25

    BOLLINGER_WINDOW = 20
    KELTNER_WINDOW = 20
    DONCHIAN_WINDOW = 20

    class Strategy:
        LONG = "long"
        SHORT = "short"
        BOTH = "both"

    def __init__(self, df, strategy=Strategy.LONG, create_fama_report=False):
        """
        df should be in right format columns datetime high low open close
        """
        super().__init__(df, create_fama_report)
        if not isinstance(df, pd.DataFrame):
            raise AttributeError("df must be DataFrame instance")

        self.df = df
        self.strategy = strategy
        self.closed_returns = df['close']
        self.rsi = rsi(df['close'], window=14)
        self.stochastic = stoch(
            df['high'], df['low'], df['close'],
            self.STOCHASTIC_HIGH, self.STOCHASTIC_LOW
        )
        self.roc = roc(df['close'], self.ROC_LENGTH)
        self.ema = ema_indicator(df['close'], self.EMA_LENGTH)
        self.sma = sma_indicator(df['close'], self.SMA_LENGTH)
        self.macd = macd(
            df['close'], self.MACD_SLOW, self.MACD_FAST
        )
        self.macd_signal = macd_signal(
            df['close'], self.MACD_SLOW, self.MACD_FAST, self.MACD_SIGN
        )
        self.ichimoku_a = ichimoku_a(
            df['high'], df['low'], self.ICHIMOKU_WINDOW_1,
            self.ICHIMOKU_WINDOW_2
        )
        self.tenkan_sen = ichimoku_conversion_line(
            df['high'], df['low'], self.ICHIMOKU_WINDOW_1,
            self.ICHIMOKU_WINDOW_2
        )
        self.kijun_sen = ichimoku_base_line(
            df['high'], df['low'], self.ICHIMOKU_WINDOW_1,
            self.ICHIMOKU_WINDOW_2
        )
        self.psar = psar_up(df['high'], df['low'], df['close'])
        self.aroon_up = aroon_up(df['close'], self.AROON_UP)
        self.aroon_down = aroon_down(df['close'], self.AROON_DOWN)
        self.wma = wma_indicator(df['close'], self.WMA_LENGTH)
        self.bollinger_low = bollinger_lband(df['close'], self.BOLLINGER_WINDOW)
        self.bollinger_high = bollinger_hband(
            df['close'], self.BOLLINGER_WINDOW
        )
        self.keltner_low = keltner_channel_lband(
            df['high'], df['low'], df['close'], self.KELTNER_WINDOW
        )
        self.keltner_high = keltner_channel_hband(
            df['high'], df['low'], df['close'], self.KELTNER_WINDOW
        )
        self.donchian_low = donchian_channel_lband(
            df['high'], df['low'], df['close'], self.DONCHIAN_WINDOW
        )
        self.donchian_high = donchian_channel_hband(
            df['high'], df['low'], df['close'], self.DONCHIAN_WINDOW
        )

    def run_rsi_long(self):
        return self.momentum_indicator_long(self.rsi, 20, 80, self.ROC_LENGTH)

    def run_stochastic_long(self):
        return self.momentum_indicator_long(
            self.stochastic, 20, 80, self.STOCHASTIC_HIGH
        )

    def run_roc_long(self):
        return self.momentum_indicator_long(self.roc, -10, 10, self.ROC_LENGTH)

    def run_ema_long(self):
        return self.trend_indicator_long(self.ema, self.EMA_LENGTH)

    def run_sma_long(self):
        return self.trend_indicator_long(self.sma, self.EMA_LENGTH)

    def run_wma_long(self):
        return self.trend_indicator_long(self.wma, self.EMA_LENGTH)

    def run_macd_long(self):
        return self.trend_double_indicator_long(
            self.macd_signal, self.macd, self.MACD_SLOW
        )

    def run_ichimoku_long(self):
        return self.trend_ichimoku_long(
            self.tenkan_sen, self.kijun_sen, self.ichimoku_a,
            self.ICHIMOKU_WINDOW_2
        )

    def run_psar_long(self):
        return self.trend_indicator_long(self.psar, 1)

    def run_aroon_long(self):
        return self.trend_double_indicator_long(
            self.aroon_down, self.aroon_up, self.AROON_DOWN
        )

    def run_bollinger_long(self):
        return self.volatility_long(
            self.bollinger_low, self.bollinger_high, self.BOLLINGER_WINDOW
        )

    def run_keltner_long(self):
        return self.volatility_long(
            self.keltner_low, self.keltner_high, self.KELTNER_WINDOW
        )

    def run_donchian_long(self):
        return self.volatility_long(
            self.donchian_low, self.donchian_high, self.DONCHIAN_WINDOW
        )

    def run_buy_and_hold(self):
        capital = self.CAPITAL
        first_price = self.df['close'][0]
        last_price = self.df['close'][len(self.df['close'])-1]
        entities = int(capital / first_price)
        result = entities * last_price
        capital += (result - capital)
        return (capital / self.CAPITAL) * 100

    def run(self):
        rsi = getattr(self, f"run_rsi_{self.strategy}")()
        stochastic = getattr(self, f"run_stochastic_{self.strategy}")()
        roc = getattr(self, f"run_roc_{self.strategy}")()
        ema = getattr(self, f"run_ema_{self.strategy}")()
        sma = getattr(self, f"run_sma_{self.strategy}")()
        wma = getattr(self, f"run_wma_{self.strategy}")()
        macd = getattr(self, f"run_macd_{self.strategy}")()
        ichimoku = getattr(self, f"run_ichimoku_{self.strategy}")()
        psar = getattr(self, f"run_psar_{self.strategy}")()
        aroon = getattr(self, f"run_aroon_{self.strategy}")()
        bollinger = getattr(self, f"run_bollinger_{self.strategy}")()
        keltner = getattr(self, f"run_keltner_{self.strategy}")()
        donchian = getattr(self, f"run_donchian_{self.strategy}")()

        return {
            "rsi": "{:.2f}".format(rsi),
            "stochastic":  "{:.2f}".format(stochastic),
            "roc": "{:.2f}".format(roc),
            "ema": "{:.2f}".format(ema),
            "sma": "{:.2f}".format(sma),
            "wma": "{:.2f}".format(wma),
            "macd": "{:.2f}".format(macd),
            "ichimoku": "{:.2f}".format(ichimoku),
            "psar": "{:.2f}".format(psar),
            "aroon": "{:.2f}".format(aroon),
            "bollinger": "{:.2f}".format(bollinger),
            "keltner": "{:.2f}".format(keltner),
            "donchian": "{:.2f}".format(donchian),
            "buy_and_hold": "{:.2f}".format(self.run_buy_and_hold()),
            "fama": self.fama_value
        }
