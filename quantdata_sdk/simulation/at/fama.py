import math
import numpy as np
import pandas as pd
import plotly.offline as opy
import plotly.graph_objs as go
from scipy import stats
from quantdata_sdk.utils import WindowException


class FamaMixin:
    WINDOW = 150
    DELAY = 10

    def __init__(self, df, create_fama_report):
        self.fama_value = None
        if create_fama_report:
            try:
                fama = Fama(df, self.WINDOW, self.DELAY)
            except WindowException:
                self.fama_value = None
            else:
                self.fama_value = fama.fama_value
            
            # self._create_fama_chart(df, fama.df)

    # def _create_fama_chart(self, df, fama_df):
    #     trace2 = go.Scatter(x=df['datetime'], y=fama_df['ljung_box'],
    #                         mode='lines', name='LjungBox')
    #     trace3 = go.Scatter(x=df['datetime'], y=fama_df['quotient_of_variance'],
    #                         mode='lines',
    #                         name='QuotientOfVariance')
    #     trace4 = go.Scatter(x=df['datetime'], y=fama_df['test_series'],
    #                         mode='lines', name='SeriesTest')

    #     length = len(df['datetime'])
    #     trace21 = go.Scatter(x=df['datetime'],
    #                          y=[stats.norm.isf(0.05) for x in range(0, length)],
    #                          name='SIW KWG', mode='line',
    #                          line=dict(color='black'))
    #     trace22 = go.Scatter(x=df['datetime'],
    #                          y=[-stats.norm.isf(0.05) for x in range(0, length)],
    #                          name='SIW KWD', mode='line',
    #                          line=dict(color='black'))
    #     # data.delay on 20
    #     trace31 = go.Scatter(x=df['datetime'],
    #                          y=[stats.chi2.isf(0.05, 20) for x in
    #                             range(0, length)],
    #                          name='LjungBox KW', mode='line',
    #                          line=dict(color='black'))

    #     trace41 = go.Scatter(x=df['datetime'],
    #                          y=[stats.norm.isf(0.05) for x in range(0, length)],
    #                          name='Series Test KWG', mode='line',
    #                          line=dict(color='black'))
    #     trace42 = go.Scatter(x=df['datetime'],
    #                          y=[-stats.norm.isf(0.05) for x in range(0, length)],
    #                          name='Series Test KWD', mode='line',
    #                          line=dict(color='black'))

    #     data = [trace2, trace3, trace4, trace21, trace22, trace31, trace41,
    #             trace42]
    #     layout = go.Layout(title='Fama',
    #                        xaxis={'title': 'date'},
    #                        yaxis={'title': 'value'})
    #     figure = go.Figure(data=data, layout=layout)
    #     opy.plot(
    #         figure, auto_open=False, output_type='div', image_height=600
    #     )


class Fama:

    def __init__(self, df, window, delay):
        self.returns = df
        self.window = window
        self.delay = delay
        self.df = get_fama_df(df=df, window=window, delay=delay)
        self.lb = 0
        self.iw = 0
        self.ts = 0
        self.fama_value = 0
        self.calculate_fama_value()

    def calculate_fama_value(self):
        lb = 0
        iw = 0
        ts = 0
        for i in range(len(self.df['datetime'])):
            try:
                if float(self.df['ljung_box'].loc[i]) > stats.chi2.isf(0.05, self.delay):
                    lb += 1
                if math.fabs(float(self.df['quotient_of_variance'].loc[i])) > stats.norm.isf(
                        0.05):
                    iw += 1
                if math.fabs(float(self.df['test_series'].loc[i])) > stats.norm.isf(
                        0.05):
                    ts += 1
            except Exception as e:
                print(str(e))
                        
        lb = lb/len(self.df['datetime'])
        iw = iw/len(self.df['datetime'])
        ts = ts/len(self.df['datetime'])
        
        self.lb = lb
        self.iw = iw
        self.ts = ts
        self.fama_value = 0.2 * lb + 0.4 * iw + 0.4 * ts


def get_fama_df(df, window=150, delay=10):
    df = df[['datetime', 'close']]
    log_returns = np.diff(np.log(df['close']))
    df = df.iloc[1:]
    df['log_returns'] = log_returns
    if len(df.index) < window: raise WindowException('window higher than log data')
    df_done = calculate_fama(df, window, delay)
    return df_done


def calculate_fama(df, window, delay):
    T = window  # 50
    len_df = len(df['close'])

    def calculate(len_df_cen, log, datetime):
        fama_indicators = []
        for i in range(1, len_df_cen - T):
            log_returns = log[i:T + i]
            log_returns_mean = np.mean(log_returns)
            ro = pd.Series(1.0, index=pd.Series(
                range(1, delay)).astype(str))
            sum_v = 0
            sum2 = 0
            n1, n2, ls, pr = 1, 1, 0, 0

            for k in range(1, delay):
                ro_up, ro_down = 0, 0
                for t2 in range(1, k):
                    ro_down += (log_returns[t2] - log_returns_mean) ** 2
                for t in range(k, T):
                    ro_up += (log_returns[t] - log_returns_mean) * \
                                 (log_returns[t - k] - log_returns_mean)
                    ro_down += (log_returns[t] - log_returns_mean) ** 2
                ro[str(k)] = (ro_up / ro_down)
                sum_v += (ro[str(k)] ** 2) / (T - k)
                sum2 += (1 - (k / (delay - 1))) * ro[str(k)]

            for ii in log_returns:
                if ii >= 0:
                    n1 += 1
                if ii < 0:
                    n2 += 1
                if ii < 0 and pr > 0 or ii > 0 and pr < 0:
                    ls += 1
                pr = ii

            E = (2 * n1 * n2 + T) / T
            S2 = (2 * n1 * n2 * (2 * n1 * n2 - T)) / ((T - 1) * (T ** 2))
            U = (ls - E) / math.sqrt(S2)

            chi2 = T * (T + 2) * sum_v
            IW = 1 + (2 * sum2)
            siw = math.sqrt(T) * (IW - 1) * ((2 * (2 * delay - 1) * (delay - 1)) / (3 * delay)) ** (-0.5) # noqa

            data = {
                "datetime": datetime[T + i],
                "auto_autocorrelation": ro,
                "ljung_box": chi2,
                "quotient_of_variance": siw,
                "test_series": U
            }
            print(f"Step: {i} of {len_df_cen}")
            fama_indicators.append(data)
        return fama_indicators

    fama_indicators = calculate(
        len_df, list(df['log_returns']), list(df['datetime'])
    )
    return pd.DataFrame(fama_indicators, columns=[
        'auto_autocorrelation', 'ljung_box', 'datetime',
        'quotient_of_variance', 'test_series'
    ])
