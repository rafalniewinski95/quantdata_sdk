HOST_URL = "quantdata.science/"

APP_URLS = {
    "graphane": "https://%s%s" % (HOST_URL, "graphql/")
}
