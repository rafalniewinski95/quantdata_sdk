"""
__version__.py
~~~~~~~~~~~~~~

Information about the current version of the quantumdata-sdk package.
"""

__title__ = 'quant-data-sdk'
__description__ = 'quant-data-sdk - sdk for quant data project'
__version__ = '0.2.3'
__author__ = 'Rafał Niewiński'
__author_email__ = 'rafalniewinski95@gmail.com'
__license__ = 'Apache 2.0'
__url__ = 'https://gitlab.com/rafalniewinski95/quantdata_sdk'