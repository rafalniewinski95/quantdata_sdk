

def get_future_value(capital=10000, r=0.05, n=1, m=1):
    """" calculated from FV = PV(1+r/m)^n*m """

    future_value = {}
    last_cash = capital
    for x in range(n):
        future_value[x+1] = {"value":  capital*(1+r/m) ** ((x+1) * m)} 
        future_value[x+1].update({"growth": 100*((future_value[x+1]["value"] / capital)-1) })
   
    return future_value 
